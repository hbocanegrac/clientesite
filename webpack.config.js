var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: [
		'react-hot-loader/patch',
		'webpack-dev-server/client?http://localhost:8081',
		'webpack/hot/only-dev-server',
		__dirname + "/src/index.js"
	],
	output: {
	path: __dirname + "/public",
	filename: "bundle.js",
	publicPath: "/",
	},
	mode: 'development',
	module : {
		rules:[
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ['es2015','react'],
					plugins: ['react-hot-loader/babel','transform-class-properties','transform-object-rest-spread']
				}
			},
			{
				test: /\.css$/,
				use: [
					{loader: "style-loader"},
					{loader: "css-loader"}
				]
			},
			{
				exclude: [/\.html$/, /\.(js|jsx)$/, /\.css$/, /\.json$/],
				loader: 'file-loader',
				options: {
					name: 'static/media/[name].[ext]'
				}
			}
		]
	},
	plugins: [
		/* new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('production')
			}
		}), */
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			inject: true,
			template: __dirname + '/public/index.html'/* ,
			minify: {
				removeComments: true,
				collapseWhitespace: true,
				removeRedundantAttributes: true,
				useShortDoctype: true,
				removeEmptyAttributes: true,
				removeStyleLinkTypeAttributes: true,
				keepClosingSlash: true,
				minifyJS: true,
				minifyCSS: true,
				minifyURLs: true,
			}, */
		})
	],
	devServer: {
		contentBase: "./public",
		historyApiFallback: true,
		inline: true,
		hot: true
	}

};

