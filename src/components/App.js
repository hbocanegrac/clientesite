import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom';
import './App.css';
import RegistroContainer from './cliente/RegistroContainer';

class App extends Component {
  state = { cliente: {} };
  render() {
    return (
      <div>
        <Route  
            exact path="/" 
            render = {() => (
                <RegistroContainer
                    user={this.state.user}
                />
            )}
        />
      </div>
    );
  }
}

export default withRouter(App);
