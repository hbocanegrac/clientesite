import React, { Component } from 'react';
import { Button, Form, Grid, Header, Message, Segment, Confirm } from 'semantic-ui-react'
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

class RegistroContainer extends Component {
    state = {  
                distritos: [], 
                provincias: [], 
                departamentos: [], 
                loading: false,
                confirma: false,
                respuesta: false,
                nombrecompleto: '',
                direccion: ''
            };
    
    constructor (props) {
        super(props)
        this.handleDateChange = this.handleDateChange.bind(this);
    }
      
    componentDidMount() {
        this.lstDepartamentos();
    }

    lstDepartamentos(){
        let lista = [];
        this.setState({ loading: true, provincias:[], distritos: [] });

        fetch('http://127.0.0.1:9080/APILugarBCO/rest/lstdepartamento', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then(response => {
            if(response.ok) {
                return response.json();
            } else {
                console.log('Error en el servidor');
                this.setState({ loading: false });
            }
        })
        .then(data => {
            lista = data.map((departamento) => {
                return {
                    key: departamento.coddepartamento,
                    value: departamento.coddepartamento,
                    text: departamento.dscdepartamento
                }
            });

            this.setState({
                departamentos: lista,
                loading: false
            });
        }).catch(error => {
            console.log('Error en el servidor: ' + error.message);
            this.setState({ loading: false });
        });
    }

    lstProvincias = coddepartamento => {
        let lista = [];
        this.setState({ loading: true, distritos:[] });

        fetch('http://127.0.0.1:9080/APILugarBCO/rest/lstprovincia/' + coddepartamento, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then(response => {
            if(response.ok) {
                return response.json();
            } else {
                console.log('Error en el servidor');
                this.setState({ loading: false });
            }
        })
        .then(data => {
            lista = data.map((provincia) => {
                return {
                    key: provincia.codprovincia,
                    value: provincia.codprovincia,
                    text: provincia.dscprovincia
                }
            });

            this.setState({
                provincias: lista,
                loading:false
            });
        })
        .catch(error => {
            console.log('Error en el servidor: ' + error.message);
            this.setState({ loading: false });
        });
    }

    lstDistritos = codprovincia => {
        let lista = [];
        this.setState({ loading: true });

        fetch('http://127.0.0.1:9080/APILugarBCO/rest/lstdistrito/' + codprovincia, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then(response => {
            if(response.ok) {
                return response.json();
            } else {
                console.log('Error en el servidor');
                this.setState({ loading: false });
            }
            
        })
        .then(data => {
            lista = data.map((distrito) => {
                return {
                    key: distrito.coddistrito,
                    value: distrito.coddistrito,
                    text: distrito.dscdistrito
                }
            });

            this.setState({
                distritos: lista,
                loading: false
            });
        })
        .catch(error => {
            console.log('Error en el servidor: ' + error.message);
            this.setState({ loading: false });
        });
    }

    onChangeProvincia = ( e, {name, value} ) => {
        this.lstDistritos(value);
    }

    onChangeDepartamento = ( e, {name, value} ) => {
        this.lstProvincias(value);
    }
    
    render() {
        return (
            <div className='login-form'>
                <Grid
                    textAlign='center'
                    style={{ height: '100%' }}
                    verticalAlign='middle'
                >
                    <Grid.Column style={{ maxWidth: 500 }}>
                        <Header as='h2' color='blue' textAlign='center'>
                            Nuevo cliente
                        </Header>
                        <Form loading = {this.state.loading} size='large' onSubmit={this.show}>
                            <Segment stacked>
                                <Form.Input
                                    fluid
                                    required
                                    placeholder='Nombre completo'
                                    onChange={this.handleFormChange.bind(this)}
                                    value={this.state.nombrecompleto}
                                    name='nombrecompleto'
                                />
                                <Form.Input fluid size='large'>
                                    <DatePicker 
                                        required
                                        className='fluid'
                                        onChange={this.handleDateChange}
                                        selected={this.state.fecnacimiento}
                                        placeholderText="Fecha de nacimiento"
                                        dateFormat="DD/MM/YYYY"
                                        isClearable={true}
                                        showYearDropdown
                                        dateFormatCalendar="MMMM"
                                        scrollableYearDropdown
                                        yearDropdownItemNumber={15}
                                    />
                                </Form.Input>  
                                <Form.Field>
                                    <Form.Select 
                                        fluid 
                                        search
                                        options ={this.state.departamentos}
                                        onChange = {this.onChangeDepartamento.bind(this)}
                                        placeholder='Departamento'
                                        value={this.state.coddepartamento}
                                    />
                                </Form.Field>
                                <Form.Select 
                                    fluid 
                                    required
                                    search
                                    options = {this.state.provincias}
                                    onChange = {this.onChangeProvincia.bind(this)}
                                    placeholder='Provincia'
                                    value={this.state.codprovincia}
                                />
                                <Form.Select 
                                    fluid
                                    required
                                    search
                                    options = {this.state.distritos}
                                    placeholder='Distrito'
                                    value={this.state.coddistrito}
                                    onChange={this.handleFormChange.bind(this)}
                                    name='coddistrito'
                                />
                                <Form.Input
                                    fluid
                                    required
                                    required
                                    placeholder='Direccion'
                                    onChange={this.handleFormChange}
                                    value={this.state.direccion}
                                    name='direccion'
                                />
                                {this.state.respuesta ? (
                                    <Message info> 
                                        <p>Datos guardados correctamente.</p>
                                    </Message>) : null
                                }
                                <Button color='blue' fluid size='large'>Registrar</Button>
                            </Segment>
                        </Form>
                        <Confirm 
                                open={this.state.confirma}
                                onCancel={this.handleCancel}
                                onConfirm={this.handleConfirm}
                                cancelButton='Cancelar'
                                content='¿Desea registrar sus datos?'
                        />
                    </Grid.Column>
                </Grid>
            </div>
            
        );
    }

    handleFormChange = (e, { name, value }) => {
        this.setState({ [name]: value })
    }

    handleConfirm = (event) => {
        this.setState({ confirma : false});   
        this.handleSubmit(event);
    }

    handleCancel = (event) => {
        this.setState({ confirma : false});   
    }

    show = () => this.setState({ confirma: true })

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({ loading: true });
        let fecha = this.state.fecnacimiento;

        const param = {
            nombrecompleto: this.state.nombrecompleto,
            fecnacimiento: (fecha ? fecha.format() : null),
            coddistrito: this.state.coddistrito,
            direccion: this.state.direccion
        };

        fetch('http://localhost:9080/APIClienteBCO/rest/registrar', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(param)
        })
        .then(response => {
            if(response.ok) {
                this.setState({ respuesta: true, loading: false });
                setTimeout(() => {
                    this.setState({ respuesta: false })
                }, 3000)
                return response;
            } else {
                console.log('Error en el servidor');
                this.setState({ loading: false });
            }
        })
        .catch(error => {
            console.log('Error en el servidor: ' + error.message);
            this.setState({ loading: false });
        });
    }
    
    handleDateChange = (date) => {
        this.setState({ fecnacimiento: date });
    };

}

export default RegistroContainer;